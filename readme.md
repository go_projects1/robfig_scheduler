https://pkg.go.dev/github.com/robfig/cron/v3#hdr-CRON_Expression_Format
```
"expression":"* * * * *" - every minuts
"expression":"0 * * * *" - every hours
"expression":"0 0 * * *" - every day 00:00
"expression":"0 */3 * * *" - every 3 hours
```