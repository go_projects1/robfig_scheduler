package schedulers

import (
	"github.com/robfig/cron/v3"
	"log"
	"robfig_scheduler/controllers"
)

type Scheduler struct {
	log  *log.Logger
	crn  *cron.Cron
	ctrl *controllers.Controller
}

func New(log *log.Logger, cron *cron.Cron, ctrl *controllers.Controller) *Scheduler {
	return &Scheduler{log: log, crn: cron, ctrl: ctrl}
}

func (s *Scheduler) CreateBackupJob(exp string) (entryId int,err error) {
	id,err := s.crn.AddFunc(exp,s.Backup)
	entryId = int(id)
	s.crn.Start()
	return
}

func (s *Scheduler) CreateRecoJob(exp string) (entryId int,err error) {
	id,err := s.crn.AddFunc(exp,s.Reconilation)
	entryId = int(id)
	s.crn.Start()
	return
}

func (s *Scheduler) RemoveJob(entryId int) {
	s.crn.Remove(cron.EntryID(entryId))
}