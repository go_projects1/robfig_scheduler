package schedulers

import "context"

func (s *Scheduler) Backup() {
	s.log.Println("backup:started")
	s.ctrl.Backup(context.TODO())
	s.log.Println("backup:ended")
}

func (s *Scheduler) Reconilation() {
	s.log.Println("reco:started")
	s.ctrl.Reco(context.TODO())
	s.log.Println("reco:ended")
}
