package rest

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"log"
	"net/http"
	"robfig_scheduler/controllers"
	"robfig_scheduler/ports/schedulers"
)

type Server struct {
	log  *log.Logger
	ctrl   controllers.Controller
	router *gin.Engine
	schedule schedulers.Scheduler
}

func New(port string, log *log.Logger,
	ctrl controllers.Controller,schedule schedulers.Scheduler) *http.Server {
	r := gin.New()
	//r.Use(cors.CORSMiddleware())
	srv := &Server{
		log: log,
		ctrl:   ctrl,
		router: r,
		schedule: schedule,
	}
	srv.endpoints()
	httpServer := &http.Server{
		Addr:    port,
		Handler: srv,
	}
	log.Println(fmt.Sprintf("HTTP server is initialized on port: %v", port))
	return httpServer
}

func (s *Server) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.router.ServeHTTP(w, r)
}