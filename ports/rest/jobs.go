package rest

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"net/http"
)

type param struct {
	Expression string `json:"expression"`
}

func (s *Server) backupStart() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := param{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity,fmt.Sprintf("input params error: %v", err.Error()))
			return
		}

		id,err := s.schedule.CreateBackupJob(req.Expression)
		if err != nil {
			c.JSON(http.StatusInternalServerError, fmt.Sprintf("Create job error: %v", err.Error()))
		}
		c.JSON(http.StatusOK,fmt.Sprintf("Id schduler: %v", id))
	}
}

func (s *Server) recoStart() gin.HandlerFunc {
	return func(c *gin.Context) {
		// Input process and Validation
		req := param{}
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusUnprocessableEntity,fmt.Sprintf("input params error: %v", err.Error()))
			return
		}
		id,err := s.schedule.CreateRecoJob(req.Expression)
		if err != nil {
			c.JSON(http.StatusInternalServerError, fmt.Sprintf("Create job error: %v", err.Error()))
		}
		c.JSON(http.StatusOK,fmt.Sprintf("Id schduler: %v", id))
	}
}

func (s *Server) removeJob() gin.HandlerFunc {
	return func(c *gin.Context) {
		id := c.Param("id")
		if id == "" {
			c.JSON(http.StatusUnprocessableEntity,fmt.Sprintf("Ne peredan id job: %v", id))
		}
		s.schedule.RemoveJob(cast.ToInt(id))

		c.JSON(http.StatusOK,fmt.Sprintf("Id schduler: %v", id))
	}
}
