package rest

func (s *Server) endpoints()  {
	actions := s.router.Group("/jobs")
	actions.POST("/backup/start",s.backupStart())
	actions.POST("/reco/start",s.recoStart())
	actions.POST("/stop/:id",s.removeJob())
}
