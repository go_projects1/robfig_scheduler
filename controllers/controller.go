package controllers

import (
	"context"
	"fmt"
	"github.com/robfig/cron/v3"
	"log"
)

type Controller struct {
	log  *log.Logger
	crn *cron.Cron
}

func New(log  *log.Logger,cron *cron.Cron) *Controller {
	return &Controller{log: log,crn: cron}
}

/*func (ctrl *Controller) AddBackupJob(ctx context.Context)  {
	ctrl.crn.AddFunc("",ctrl.Backup())
}*/

func (ctrl *Controller) Backup(ctx context.Context)  {
	fmt.Println("Started backup")
	fmt.Println("Ended backup")
}

func (ctrl *Controller) Reco(ctx context.Context)  {
	fmt.Println("Started reco")
	fmt.Println("Ended reco")
}