package main

import (
	"context"
	"fmt"
	"github.com/robfig/cron/v3"
	"log"
	"net/http"
	"os"
	"os/signal"
	"robfig_scheduler/controllers"
	"robfig_scheduler/ports/rest"
	"robfig_scheduler/ports/schedulers"
)

type App struct {
	cron *cron.Cron
}

func main() {
	quitSignal := make(chan os.Signal, 1)
	signal.Notify(quitSignal, os.Interrupt)
	l := log.Default()
	ctx, cancel := context.WithCancel(context.Background())
	go func() {
		OSCall := <-quitSignal
		l.Println(fmt.Sprintf("System Call: %+v", OSCall))
		cancel()
	}()
	app := App{}

	cr := cron.New()
	ctrl := controllers.New(l, cr)
	schdls := schedulers.New(l, cr, ctrl)
	srv := rest.New(":8696", l, *ctrl, *schdls)
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			panic(err)
		}
	}()
	defer cr.Stop()
	app.cron = cr
	<-ctx.Done()
}
